import Colors from 'babapp/src/constants/Colors'

module.exports = {
    iconSize: 20,
    borderWidth: 0.7,
    borderColor: Colors.neutralColor
}
