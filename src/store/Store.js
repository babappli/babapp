import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import {
    AccountReducer,
    AccountStore,
} from 'babapp/src/account/reducers/AccountReducer'
import {
    MapReducer,
    MapStore,
} from 'babapp/src/ranking/reducers/MapReducer'
import {
    ScannerReducer,
    ScannerStore,
} from 'babapp/src/scanner/reducers/ScannerReducer'
import {
    RESET_ALL_STORE
} from 'babapp/src/constants/GlobalConstants'

const INITIAL_STATE = {
    AccountReducer: AccountStore,
    MapReducer: MapStore,
    ScannerReducer: ScannerStore,
}

const middlewares = [
    thunk,
]

const enhancer = applyMiddleware(...middlewares)

const CombinedReducers = combineReducers({
    AccountReducer,
    MapReducer,
    ScannerReducer,
})

const RootReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case RESET_ALL_STORE:
            return Object.assign({}, INITIAL_STATE)
        default:
            return state
    }
}

const Store = createStore(CombinedReducers, INITIAL_STATE, enhancer)

export default Store
