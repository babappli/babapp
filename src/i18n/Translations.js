import { fr } from 'babapp/src/i18n/fr'
import { en } from 'babapp/src/i18n/en'

module.exports = {
    fr,
    en,
}
