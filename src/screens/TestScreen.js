import React from 'react'
import { ScrollView, StyleSheet, Text } from 'react-native'

export default class Test extends React.Component {
    static navigationOptions = {
        title: 'Test',
    }

    render() {
        return (
            <ScrollView style={ styles.container }>
                <Text>This is a fucking Test</Text>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
})
