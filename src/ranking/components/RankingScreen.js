import React from 'react'
import {
    FlatList,
    StyleSheet,
    View,
    Text,
} from 'react-native'
import { Icon } from 'expo'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import i18n from 'i18n-js'

import Store from 'babapp/src/store/Store'
import RankingActions from 'babapp/src/ranking/actions/RankingActions'
import UserDto from 'babapp/src/ranking/dto/UserDto'
import SharedStyle from 'babapp/src/constants/SharedStyle'
import { icon } from 'babapp/src/utils/IconUtils'
import ListIcon from 'babapp/src/components/icons/ListIcon'

class RankingScreen extends React.Component {
    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)
        Store.dispatch(RankingActions.fetchScores())
        this.state = {
            refreshing: false,
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true})
        Store.dispatch(RankingActions.fetchScores(() => {
            this.setState({refreshing: false})
        }))
    }

    _scorePanel = ({ item }) => {
        return (
          <View style={styles.item}>
            <Text style={[styles.itemText, styles.login]}>{item.login}</Text>
            <Text style={[styles.itemText, styles.win]}>{item.win}</Text>
            <Text style={[styles.itemText, styles.lose]}>{item.lose}</Text>
          </View>
        )
    }

    _rankPanel = (user, scores) => {
        const rank = scores.findIndex(score => score.login == user.login)
        if (rank != -1) {
            const { win, lose } = scores[rank]
            return (
                <View style={styles.itemRank}>
                    <Icon.Ionicons name={ icon('ribbon') } style={ styles.medal } color={ '#d0b0b9' } size={ 40 }/>
                    <Text style={[styles.itemText, styles.rank]}>{rank + 1}</Text>
                    <Text style={[styles.itemText, styles.loginRank]}>{ user.login }</Text>
                    <View style={ styles.score }>
                        <View style={ [styles.rankCircles, styles.winRank] }>
                            <Text style={ styles.rankText }>{ win }</Text>
                        </View>
                        <View style={ [styles.rankCircles, styles.loseRank] }>
                            <Text style={ styles.rankText }>{ lose }</Text>
                        </View>
                    </View>
                </View>
            )
        }
        return (
            <View style={styles.itemRank}>
                <Icon.Ionicons name={ icon('pulse') } style={ styles.medal } color={ '#d0b0b9' } size={ 40 }/>
                <Text style={[styles.itemText, styles.loginRank]}>{i18n.t('sorryYouNeverPlayed')}</Text>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={ styles.titleContainer }>
                    <Text style={ styles.title }>{i18n.t('ranking')}</Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.overflow}>
                        {this._rankPanel(this.props.user, this.props.scores)}
                        <View style={styles.itemHeader}>
                            <Text style={[styles.head, styles.loginHeader]}>{i18n.t('login')}</Text>
                            <Text style={[styles.head, styles.winHeader]}>{i18n.t('win')}</Text>
                            <Text style={[styles.head, styles.loseHeader]}>{i18n.t('lose')}</Text>
                        </View>
                        <FlatList
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                            data={ this.props.scores }
                            renderItem={ this._scorePanel }
                            keyExtractor={ i => i.login }
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    titleContainer: {
        paddingTop: 50,
        paddingBottom: 50,
        position: 'relative',
        zIndex: 1,
        backgroundColor: '#d0b0b9',
    },
    title: {
        color: '#f1e7ea',
        fontSize: 50,
        fontFamily: 'lobster',
        textAlign: 'center',
        textShadowColor: 'rgba(0,0,0,0.4)',
        textShadowOffset: { widht: 1, height: 1 },
        textShadowRadius: 10,
    },
    content: {
        flex: 1,
        position: 'relative',
        zIndex: 2,
        top: -30,
        marginRight: 20,
        marginLeft: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: { widht: 0, height: 3 },
        shadowRadius: 10,
        shadowOpacity: 0.2,
        elevation: 4,
        bottom: 0,
    },
    overflow: {
        overflow: 'hidden',
        flex: 1,
    },
    itemRank: {
        borderRadius: 10,
        padding: 20,
        paddingLeft: 5,
        paddingRight: 5,
        margin: 20,
        marginTop: 5,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    medal: {
        flex: 3,
        textAlign: 'left',
    },
    rank: {
        textAlign: 'center',
        flex: 2,
        fontFamily: 'lobster',
        fontSize: 25,
    },
    loginRank: {
        flex: 9,
        color: '#d0b0b9',
        fontFamily: 'lobster',
        fontSize: 25,
    },
    score: {
        flex: 3,
        alignItems: 'center',
    },
    rankCircles: {
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        borderRadius: 100,
        width: 30,
        height: 30,
    },
    winRank: {
        marginBottom: 5,
        backgroundColor: '#7eb985',
    },
    loseRank: {
        marginTop: 5,
        backgroundColor: '#b97e7e',
    },
    rankText: {
        textAlign: 'center',
        color: 'white',
    },
    itemHeader: {
        flexDirection: 'row',
        padding: 20,
        marginRight: 10,
        marginLeft: 10,
    },
    head: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#d0b0b9',
    },
    loginHeader: {
        flex: 6,
        textAlign: 'left',
    },
    winHeader: {
        flex: 2,
    },
    loseHeader: {
        flex: 2,
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        borderTopWidth: SharedStyle.borderWidth,
        borderTopColor: SharedStyle.borderColor,
        padding: 20,
        marginRight: 10,
        marginLeft: 10,
    },
    itemText: {
      textAlign: 'center',
      fontSize: 18,
    },
    login: {
      flex: 6,
      textAlign: 'left'
    },
    win: {
      flex: 2,
    },
    lose: {
      flex: 2,
    },
})

RankingScreen.propTypes = {
    user: PropTypes.object,
    scores: PropTypes.arrayOf(PropTypes.object),
}

const mapStateToProps = store => ({
    user: store.AccountReducer.user,
    scores: store.MapReducer.scores,
})

export default connect(mapStateToProps)(RankingScreen)
