class _UserDto {
    constructor(obj) {
        this.id = obj.id
        this.login = obj.login
        this.email = obj.email
        this.pass = obj.pass
        this.role = obj.role
    }
}

export default function UserDto(user) {
    return new _UserDto(Object.assign({ user: {} }, user))
}
