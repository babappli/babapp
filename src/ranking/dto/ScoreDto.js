class _ScoreDto {
  constructor(obj) {
    this.login = obj.login
    this.win = obj.win
    this.lose = obj.lose
  }
}

export default function ScoreDto (score) {
  return new _ScoreDto(Object.assign({ score : {}}, score))
}
