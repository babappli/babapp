import {
    RECEIVE_USERS,
    RECEIVE_SCORES,
} from 'babapp/src/ranking/constants/RankingConstants'
import ApiRoutes from 'babapp/src/navigation/constants/ApiRoutes'

const receiveUsers = users => ({ type: RECEIVE_USERS, users })
const receiveScores = scores => ({ type: RECEIVE_SCORES, scores })

const fetchUsers = () => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.users())
        if (!res.ok && (res.status < 200 || res.status >= 300))
            throw new Error(res.statusText)

        const users = await res.json()
        if (!users)
            throw new Error(res.statusText)

        dispatch(receiveUsers(users))
    } catch (err) {
        console.log(err)
    }
}

const fetchScores = (cb = () => {}) => async dispatch => {
  try {
    const res = await fetch(ApiRoutes.scores())
    if (!res.ok && (res.status < 200 || res.status >= 300))
      throw new Error(res.statusText)

    const scores = await res.json()

    if (!scores)
      throw new Error(res.statusText)

    dispatch(receiveScores(scores))
  } catch (err) {
    console.log(err)
  }
  cb()
}

export default { fetchUsers, fetchScores }
