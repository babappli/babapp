import MapConstants from 'babapp/src/ranking/constants/RankingConstants'
import UserDto from 'babapp/src/ranking/dto/UserDto'
import ScoreDto from 'babapp/src/ranking/dto/ScoreDto'

export const MapStore = {
    users: [],
    scores: []
}

export function MapReducer(state = MapStore, action) {
    switch (action.type) {
        case MapConstants.RECEIVE_USERS:
            return Object.assign({}, state, {
                users: action.users.map(u => UserDto(u))
            })
        case MapConstants.RECEIVE_SCORES:
            return Object.assign({}, state, {
                scores: action.scores.map(s => ScoreDto(s))
            })
        default:
            return state
    }
}
