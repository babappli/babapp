import { Platform } from 'react-native'

const icon = name => Platform.OS === 'ios' ? `ios-${name}` : `md-${name}`

export { icon }
