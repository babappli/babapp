module.exports = {
    AUTH: 'Auth',
    LOGIN: 'Login',
    HOME: 'Home',
    RESEARCH: 'Research',
    ACCOUNT: 'Account',
    PASSWORD: 'Password',
    EMAIL: 'Email',
    TEST: 'Test',
    SIGNIN: 'Signin',
    SIGNUP: 'Signup',
}
