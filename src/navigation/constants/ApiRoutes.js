import {
    path,
} from 'babapp/src/conf/Conf'

export default {
    signin: () => `${path}/rpc/login`,
    users: (params = '') => `${path}/users${params}`,
    bab: (params = '') => `${path}/bab${params}`,
    game: (params = '') => `${path}/game${params}`,
    team: (params = '') => `${path}/team${params}`,
    gameTeam: (params = '') => `${path}/game_team${params}`,
    scores: () => `${path}/ranking_view`
}
