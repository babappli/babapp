import React from 'react'
import { createSwitchNavigator, createNavigationContainer } from 'react-navigation'

import AuthScreen from 'babapp/src/auth/components/AuthScreen'
import AuthNavigation from 'babapp/src/navigation/AuthNavigator'
import MainTabNavigator from 'babapp/src/navigation/MainTabNavigator'

export default createNavigationContainer(createSwitchNavigator({
    Auth: AuthScreen,
    Login: AuthNavigation,
    Main: MainTabNavigator,
}, {
    initialRouteName: 'Auth',
}))
