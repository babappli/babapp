import React from 'react'
import { createStackNavigator } from 'react-navigation'

import SigninScreen from 'babapp/src/auth/components/SigninScreen'
import SignupScreen from 'babapp/src/auth/components/SignupScreen'

export default createStackNavigator({
    Signin: SigninScreen,
    Signup: SignupScreen,
})
