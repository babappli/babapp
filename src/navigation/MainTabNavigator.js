import React from 'react'
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation'

import TabBarIcon from 'babapp/src/components/icons/TabBarIcon'
import ResearchScreen from 'babapp/src/ranking/components/RankingScreen'
import ScannerScreen from 'babapp/src/scanner/components/ScannerScreen'
import AccountPassword from 'babapp/src/account/components/AccountPassword'
import AccountEmail from 'babapp/src/account/components/AccountEmail'
import AccountScreen from 'babapp/src/account/components/AccountScreen'
import Colors from 'babapp/src/constants/Colors'
import { icon } from 'babapp/src/utils/IconUtils'
import Routes from 'babapp/src/navigation/constants/Routes'

const ResearchStack = createStackNavigator({
    Research: ResearchScreen,
})

ResearchStack.navigationOptions = {
    tabBarLabel: 'Research',
    tabBarOptions: {
        showLabel: false,
        inactiveTintColor: Colors.secondaryColor,
    },
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={ focused }
            name={ icon('list') }
        />
    ),
}

const ScanneStack = createStackNavigator({
    Scanne: ScannerScreen,
})

ScanneStack.navigationOptions = {
    tabBarLabel: 'Scanne',
    tabBarOptions: {
        showLabel: false,
        inactiveTintColor: Colors.secondaryColor,
    },
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={ focused }
            name={ icon('play') }
        />
    ),
}

const AccountStack = createStackNavigator({
    Account: AccountScreen,
    Password: AccountPassword,
    Email: AccountEmail,
})

AccountStack.navigationOptions = {
    tabBarLabel: 'Account',
    tabBarOptions: {
        showLabel: false,
        inactiveTintColor: Colors.secondaryColor,
    },
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={ focused }
            name={ icon('person') }
        />
    ),
}

export default createBottomTabNavigator({
    ResearchStack,
    ScanneStack,
    AccountStack,
})
