import React from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet,
    Text,
    TouchableOpacity,
} from 'react-native'

export default class Button extends React.Component {
    render() {
        return (
            <TouchableOpacity
                style={ [styles.container, this.props.style] }
                onPress={ this.props.onPress }
            >
                <Text
                    style={{ color: this.props.color }}
                >{ this.props.title }</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        borderRadius: 100,
        paddingTop: 15,
        paddingBottom: 15,
        marginBottom: 15,
    },
})

Button.propTypes = {
    title: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
    ]).isRequired,
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    onPress: PropTypes.func,
    color: PropTypes.string,
}

Button.defaultProps = {
    title: '',
    color: '#d0b0b9',
    style: {},
    onPress: () => {},
}
