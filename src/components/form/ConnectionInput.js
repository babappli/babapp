import React from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet,
    TextInput,
} from 'react-native'

export default class ConnectionInput extends React.Component {
    render() {
        return (
            <TextInput
                { ...this.props }
                style={ [styles.input, this.props.style] }
            />
        )
    }
}

const styles = StyleSheet.create({
    input: {
        marginBottom: 15,
        borderRadius: 80,
        padding: 15,
    },
})

ConnectionInput.propTypes = {
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
}

ConnectionInput.defaultProps = {
    style: {},
}
