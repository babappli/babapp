import React from 'react'
import {
    StyleSheet,
    View,
    TextInput,
} from 'react-native'
import PropTypes from 'prop-types'
import { LinearGradient } from 'expo'

import Colors from 'babapp/src/constants/Colors'

export default class GradientHeader extends React.Component {
    render() {
        return (
            <View style={ styles.container }>
                <LinearGradient
                    colors={ this.props.colors }
                    style={ [styles.gradientContainer, this.props.style] }
                >
                    { this.props.children || this.props.component || null }
                </LinearGradient>
            </View>
        )
    }
}

GradientHeader.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
    ]).isRequired,
    component: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
    ]),
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
    colors: PropTypes.array,
    start: PropTypes.array,
    end: PropTypes.array,
    locations: PropTypes.array,
}

GradientHeader.defaultProps = {
    children: null,
    component: null,
    style: {},
    colors: [Colors.mainColor, 'transparent'],
    locations: [0, 100],
}

const styles = StyleSheet.create({
    gradientContainer: {
    },
})
