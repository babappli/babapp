import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'expo'
import { StyleSheet } from 'react-native'

import { mainColor } from 'babapp/src/constants/Colors'
import { iconSize } from 'babapp/src/constants/SharedStyle'

export default class ListIcon extends React.Component {
    render() {
        return (
            <Icon.Ionicons
                name={ this.props.name }
                style={ [styles.icon, this.props.style] }
                color={ this.props.color }
                size={ this.props.size }
            />
        )
    }
}

const styles = StyleSheet.create({
    icon: {
        marginBottom: -3,
    }
})

ListIcon.propTypes = {
    color: PropTypes.string,
    size: PropTypes.number,
    style: PropTypes.object,
}

ListIcon.defaultProps = {
    color: mainColor,
    size: iconSize,
    style: {},
}
