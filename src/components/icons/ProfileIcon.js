import React from 'react'
import PropTypes from 'prop-types'
import {
    Platform,
    StyleSheet,
    View,
    Image,
} from 'react-native'
import { Icon } from 'expo'

import Layout from 'babapp/src/constants/Layout'
import Colors from 'babapp/src/constants/Colors'
import SharedStyle from 'babapp/src/constants/SharedStyle'

export default class ProfileIcon extends React.Component {
    render() {
        const size = this.props.size
        return (
            <View style={ [styles.container, {
                width: size,
                height: size,
                borderRadius: size/2,
            }] }>
                {
                    this.props.picture ? (
                        <Image
                            style={{ width: size, height: size }}
                            source={{ uri: this.props.picture }}
                        />
                    ) : (
                        <Icon.Ionicons
                            name={ Platform.OS === 'ios' ? 'ios-person' : 'md-person' }
                            size={ size/2 }
                            style={ styles.icon }
                            color={ Colors.mainColor }
                        />
                    )
                }
            </View>
        )
    }
}

ProfileIcon.propTypes = {
    size: PropTypes.number,
    picture: PropTypes.string,
}

ProfileIcon.defaultProps = {
    size: Layout.window.width/4,
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.lightColor,
        borderColor: Colors.secondaryColor,
        borderWidth: 1,
    },
    icon: {
        marginBottom: -3,
    },
})
