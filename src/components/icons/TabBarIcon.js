import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'expo'

import Colors from 'babapp/src/constants/Colors'

export default class TabBarIcon extends React.Component {
    render() {
        return (
            <Icon.Ionicons
                name={ this.props.name }
                size={ 26 }
                style={{ marginBottom: -3 }}
                color={ !this.props.focused ? Colors.secondaryColor : Colors.mainColor }
            />
        )
    }
}

TabBarIcon.propTypes = {
    focused: PropTypes.bool
}
