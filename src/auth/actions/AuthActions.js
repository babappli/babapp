import { SecureStore } from 'expo'

import {
    LOGIN,
} from 'babapp/src/auth/constants/AuthConstants'
import { btoa } from 'babapp/src/utils/Base64Utils'
import {
    USER_TOKEN,
    EMAIL,
    PASSWORD,
} from 'babapp/src/constants/GlobalConstants'
import Routes from 'babapp/src/navigation/constants/Routes'
import ApiRoutes from 'babapp/src/navigation/constants/ApiRoutes'
import { navigate } from 'babapp/src/navigation/actions/NavigatorActions'
import AccountActions from 'babapp/src/account/actions/AccountActions'
import {
    RESET_ALL_STORE
} from 'babapp/src/constants/GlobalConstants'

const signin = ({ email, pass }) => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.signin(), {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ email, pass })
        })
        if (!res.ok && (res.status < 200 || res.status >= 300)) {
            throw new Error(res.statusText)
        }
        const json = await res.json()
        if (!json && !json.length) {
            throw new Error(res.statusText)
        }
        const key = Object.keys(json[0])[0]
        const token = json[0][key]
        await SecureStore.setItemAsync(USER_TOKEN, token)
        await SecureStore.setItemAsync(EMAIL, email)
        await SecureStore.setItemAsync(PASSWORD, pass)

        dispatch(AccountActions.fetchUser(email))

        navigate(Routes.RESEARCH)
    } catch (err) {
        console.log(err)
    }
}

const signup = ({ login, email, pass }) => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.users(), {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ email, pass, login, role: 'webuser' })
        })

        if (!res.ok && (res.status < 200 || res.status >= 300)) {
            throw new Error(res.statusText)
        }
        navigate(Routes.SIGNIN)
    } catch (err) {
        console.log(err)
    }
}

const resetStore = () => ({
    type: RESET_ALL_STORE
})

const signout = () => async dispatch => {
    await SecureStore.deleteItemAsync(USER_TOKEN)
    await SecureStore.deleteItemAsync(EMAIL)
    dispatch(resetStore())
    navigate(Routes.SIGNIN)
}

export default { signin, signup, signout }
