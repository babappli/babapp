import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native'
import { LinearGradient } from 'expo'
import i18n from 'i18n-js'
import { Header } from 'react-navigation'

import Routes from 'babapp/src/navigation/constants/Routes'
import Button from 'babapp/src/components/buttons/Button'
import ConnectionInput from 'babapp/src/components/form/ConnectionInput'
import Store from 'babapp/src/store/Store'
import Colors from 'babapp/src/constants/Colors'
import AuthActions from 'babapp/src/auth/actions/AuthActions'
import { Icon } from 'expo'
import { icon } from 'babapp/src/utils/IconUtils'

export default class SignupScreen extends React.Component {
    static navigationOptions = {
        //title: 'Welcome'
        header: null
    }

    state = {
        login: '',
        email: '',
        pass: '',
    }

    _handleSignup = () => {
        Store.dispatch(AuthActions.signup(this.state))
    }

    _handleGoBack = () => {
        this.props.navigation.goBack()
    }

    _handleChange = (attr, value) => {
        this.setState(prevState => {
            prevState[attr] = value
            return prevState
        })
    }

    render() {
        return (
            <View style={ styles.container }>
                <TouchableOpacity onPress={ this._handleGoBack } style={ styles.close }>
                    <View>
                        <Icon.Ionicons
                            name={ icon('arrow-back') }
                            style={ styles.play }
                            color={ '#fff' }
                            size={ 30 }
                        />
                    </View>
                </TouchableOpacity>
                <View style={ styles.effect }>
                    <Text style={ styles.title }>{i18n.t('welcome')}</Text>
                </View>
                <View style={ styles.formContainer }>
                    <ConnectionInput
                        style={ styles.input }
                        value={ this.state.email }
                        onChangeText={ t => this._handleChange('email', t) }
                        placeholder={ i18n.t('email') }
                    />
                    <ConnectionInput
                        style={ styles.input }
                        value={ this.state.login }
                        onChangeText={ t => this._handleChange('login', t) }
                        placeholder={ i18n.t('login') }
                    />
                    <ConnectionInput
                        secureTextEntry={ true }
                        style={ styles.input }
                        value={ this.state.pass }
                        onChangeText={ t => this._handleChange('pass', t) }
                        placeholder={ i18n.t('password') }
                    />
                </View>
                <View style={ styles.bottomContainer } >
                    <Button
                        style={ styles.button }
                        onPress={ this._handleSignup }
                        title={ i18n.t('signup') }
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
    },
    close: {
        position: 'absolute',
        top: 30,
        left: 30,
        zIndex: 3,
    },
    title: {
        padding: 30,
        color: '#f1e7ea',
        fontSize: 60,
        fontFamily: 'lobster',
        textAlign: 'center',
        textShadowColor: 'rgba(0, 0, 0, 0.4)',
        textShadowOffset: { width: 1, height: 1 },
        textShadowRadius: 10,
    },
    effect: {
        paddingTop: 60,
        paddingBottom: 50,
        backgroundColor: '#d0b0b9',
        position: 'relative',
        zIndex: 1,
        top: 0,
    },
    formContainer: {
        borderRadius: 10,
        zIndex: 2,
        backgroundColor: 'white',
        position: 'relative',
        top: -35,
        alignItems: 'stretch',
        justifyContent: 'center',
        marginBottom: 40,
        marginRight: 25,
        marginLeft: 25,
        paddingTop: 35,
        paddingRight: 25,
        paddingLeft: 25,
    },
    input: {
        borderWidth: 1,
        borderColor: 'transparent',
        backgroundColor: 'rgba(208, 176, 185, 0.3)',
        color: '#d0b0b9',
    },
    bottomContainer: {
        paddingRight: 50,
        paddingLeft: 50,
    },
    button: {
        borderWidth: 1,
        borderColor: 'transparent',
        backgroundColor: 'rgba(208, 176, 185, 0.1)',
        color: '#d0b0b9',
    },
})
