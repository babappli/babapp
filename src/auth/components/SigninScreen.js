import React from 'react'
import {
    ScrollView,
    StyleSheet,
    View,
    Text,
} from 'react-native'
import i18n from 'i18n-js'

import Routes from 'babapp/src/navigation/constants/Routes'
import Button from 'babapp/src/components/buttons/Button'
import ConnectionInput from 'babapp/src/components/form/ConnectionInput'
import Store from 'babapp/src/store/Store'
import AuthActions from 'babapp/src/auth/actions/AuthActions'

export default class SigninScreen extends React.Component {
    static navigationOptions = {
        header: null,
    }

    state = {
        email: '',
        pass: '',
    }

    _handleLogin = () => {
        Store.dispatch(AuthActions.signin(this.state))
    }

    _handleSignup = () => {
        this.props.navigation.navigate(Routes.SIGNUP)
    }

    _handleChange = (attr, value) => {
        this.setState(prevState => {
            prevState[attr] = value
            return prevState
        })
    }

    render() {
        return (
            <View style={ styles.container }>
                <ScrollView style={ styles.container } contentContainerStyle={ styles.contentContainer }>
                    <View style={ styles.brandContainer }>
                        <Text style={ styles.brandTitle }>{ `Bab'App` }</Text>
                    </View>
                    <View style={ styles.formContainer }>
                        <ConnectionInput
                            style={ styles.input }
                            value={ this.state.email }
                            onChangeText={ t => this._handleChange('email', t) }
                            placeholder={ i18n.t('emailOrLogin') }
                        />
                        <ConnectionInput
                            secureTextEntry={ true }
                            style={ styles.input }
                            value={ this.state.pass }
                            onChangeText={ t => this._handleChange('pass', t) }
                            placeholder={ i18n.t('password') }
                        />
                    </View>
                    <View style={ styles.bottomContainer } >
                        <Button
                            style={ styles.button }
                            onPress={ this._handleLogin }
                            title={ i18n.t('signin') }
                        />
                        <Button
                            style={ [styles.button] }
                            onPress={ this._handleSignup }
                            title={ i18n.t('signup') }
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contentContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingTop: 30,
    },
    brandContainer: {
        flex: 2,
        alignItems: 'center',
        marginTop: 40,
        marginBottom: 20,
    },
    brandTitle: {
        color: '#d0b0b9',
        fontSize: 60,
        fontFamily: 'lobster',
    },
    formContainer: {
        flex: 3,
        alignItems: 'stretch',
        justifyContent: 'center',
        marginTop: 40,
        marginBottom: 40,
        paddingRight: 50,
        paddingLeft: 50,
    },
    input: {
        borderWidth: 1,
        borderColor: 'transparent',
        backgroundColor: 'rgba(208, 176, 185, 0.3)',
        color: '#d0b0b9',
    },
    bottomContainer: {
        flex: 3,
        paddingRight: 50,
        paddingLeft: 50,
    },
    button: {
        borderWidth: 1,
        borderColor: 'transparent',
        backgroundColor: 'rgba(208, 176, 185, 0.1)',
    },
})
