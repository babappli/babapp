import React from 'react'
import {
    ActivityIndicator,
    StyleSheet,
    View,
} from 'react-native'
import { SecureStore } from 'expo'

import { USER_TOKEN } from 'babapp/src/constants/GlobalConstants'
import Routes from 'babapp/src/navigation/constants/Routes'

export default class AuthScreen extends React.Component {
    constructor(props) {
        super(props)
        this._bootstrapAsync()
    }

    _bootstrapAsync = async () => {
        await SecureStore.deleteItemAsync(USER_TOKEN)
        const userToken = await SecureStore.getItemAsync(USER_TOKEN)
        this.props.navigation.navigate(userToken ? Routes.HOME : Routes.SIGNIN)
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
