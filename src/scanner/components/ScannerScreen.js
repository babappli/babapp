import React from 'react'
import PropTypes from 'prop-types'
import {
    LayoutAnimation,
    Text,
    View,
    StyleSheet,
    Modal,
    ScrollView,
    RefreshControl,
} from 'react-native'
import { connect } from 'react-redux'

import PlayComponent from 'babapp/src/scanner/components/PlayComponent'
import GameComponent from 'babapp/src/scanner/components/GameComponent'
import ScannerComponent from 'babapp/src/scanner/components/ScannerComponent'
import ScannerActions from 'babapp/src/scanner/actions/ScannerActions'
import RankingActions from 'babapp/src/ranking/actions/RankingActions'
import Store from 'babapp/src/store/Store'

class ScannerScreen extends React.Component {
    static navigationOptions = {
        header: null
    }

    state = {
        bab: null,
        team: null,
        modalVisible: false,
        refreshing: false,
    }

    componentDidMount() {
        Store.dispatch(RankingActions.fetchUsers())
        Store.dispatch(ScannerActions.fetchYourGame(this.props.user.id))
    }

    _onRefresh = () => {
        this.setState({ refreshing: true })
        Store.dispatch(ScannerActions.fetchYourGame(this.props.user.id, () => {
            this.setState({ refreshing: false })
        }))
    }

    componentDidUpdate() {
        if (this.props.game) {
            setTimeout(() => {
                Store.dispatch(ScannerActions.fetchYourGame(this.props.user.id))
            }, 1500)
        }
    }

    _handleBarCodeRead = result => {
        if (result.data) {
            try {
                const json = JSON.parse(result.data)
                if (json.bab && json.team) {
                    Store.dispatch(ScannerActions.availableBab(json))
                    .then((t) => {
                        LayoutAnimation.spring()
                        this.setState({ bab: json.bab, team: json.team, modalVisible: true })
                    })
                }
            } catch(err) {
                console.log(err)
            }
        }
    }

    _getContent = () => {
        if (this.props.game && this.props.game.id) {
            return <GameComponent user={ this.props.user }/>
        }
        return <ScannerComponent handleBarCodeRead={ this._handleBarCodeRead }/>
    }

    render() {
        return (
            <ScrollView
                contentContainerStyle={ styles.container }
                refreshControl={
                    <RefreshControl
                        refreshing={ this.state.refreshing }
                        onRefresh={ this._onRefresh }
                    />
                }
            >
                <View style={ styles.container }>
                    <Modal
                        animationType='slide'
                        transparent={ false }
                        visible={ this.state.modalVisible }
                        onRequestClose={() => Alert.alert('Modal has been closed.')}
                    >
                        <PlayComponent babGame={ this.props.babGame } bab={ this.state.bab } team={ this.state.team } onExit={ () => this.setState({ modalVisible: false }) }/>
                    </Modal>
                    { this._getContent() }
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
    },
})

ScannerScreen.propTypes = {
    user: PropTypes.object,
    game: PropTypes.object,
    babGame: PropTypes.object,
}

const mapStateToProps = store => ({
    user: store.AccountReducer.user,
    game: store.ScannerReducer.game,
    babGame: store.ScannerReducer.babGame,
})

export default connect(mapStateToProps)(ScannerScreen)
