import React from 'react'
import PropTypes from 'prop-types'
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
} from 'react-native'
import {
    BarCodeScanner,
    Permissions,
} from 'expo'

class ScannerComponent extends React.Component {
    state = {
        hasCameraPermission: null,
    }

    componentDidMount() {
        this._requestCameraPermission()
    }

    _requestCameraPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA)
        this.setState({
            hasCameraPermission: status === 'granted',
        })
    }

    render() {
        return (
            <View style={ styles.container }>
                {
                    this.state.hasCameraPermission === null ? (
                        <Text styles={ styles.text }>Requesting for camera permission</Text>
                    ) : this.state.hasCameraPermission === false ? (
                        <Text styles={ styles.text }>Camera permission is not granted</Text>
                    ) : (
                        <BarCodeScanner
                            onBarCodeRead={ this.props.handleBarCodeRead }
                            style={ {
                                height: Dimensions.get('window').height,
                                width: Dimensions.get('window').width,
                            } }
                        />
                    )
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        backgroundColor: 'white',
        alignSelf: 'stretch',
    },
    text: {
        textAlign: 'center',
        fontFamily: 'lobster',
        fontSize: 40,
    },
})

export default ScannerComponent
