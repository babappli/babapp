import React from 'react'
import {
    StyleSheet,
    View,
    Text,
} from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import i18n from 'i18n-js'

class GameComponent extends React.Component {
    _getTeam = color => {
        const team = this.props.team.filter(t => t.color == color)
        if (team.length) {
            return team.map((t,i) => {
                const user = this.props.users.find(u => t.users_id == u.id)
                return user ? <Text key={i} style={ styles.user }>{ user.login }</Text> : <Text key={i}>{i18n.t('unknownPlayer')}</Text>
            })
        }
        return <Text>{i18n.t('noPlayersKnown')}</Text>
    }

    render() {
        return (
            <View style={ styles.container }>
                <View style={ styles.blue }>
                    <Text style={ styles.title }>{i18n.t('bleuTeam')}</Text>
                    <Text style={ styles.score }>{ this.props.game.scoreblue }</Text>
                    { this._getTeam('blue') }
                </View>
                <View style={ styles.red }>
                    <Text style={ styles.title }>{i18n.t('redTeam')}</Text>
                    <Text style={ styles.score }>{ this.props.game.scorered }</Text>
                    { this._getTeam('red') }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
    },
    blue: {
        backgroundColor: '#9b9fd2',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
    },
    red: {
        backgroundColor: '#b97e7e',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
    },
    score: {
        fontSize: 25,
        marginTop: 20,
        marginBottom: 15,
    },
    title: {
        fontFamily: 'lobster',
        fontSize: 34,
    },
    user: {
        marginBottom: 10,
    }
})

GameComponent.propTypes = {
    game: PropTypes.object,
    team: PropTypes.arrayOf(PropTypes.object),
    users: PropTypes.arrayOf(PropTypes.object),
    user: PropTypes.object,
}

const mapStateToProps = store => ({
    game: store.ScannerReducer.game,
    team: store.ScannerReducer.team,
    users: store.MapReducer.users,
})

export default connect(mapStateToProps)(GameComponent)
