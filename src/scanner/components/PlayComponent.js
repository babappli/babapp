import React from 'react'
import {
    TouchableOpacity,
    View,
    Text,
    StyleSheet,
} from 'react-native'
import { Icon } from 'expo'
import PropTypes from 'prop-types'
import i18n from 'i18n-js'

import { icon } from 'babapp/src/utils/IconUtils'
import ScannerActions from 'babapp/src/scanner/actions/ScannerActions'
import Store from 'babapp/src/store/Store'

class PlayComponent extends React.Component {
    _handlePlay = () => {
        Store.dispatch(ScannerActions.joinGame(this.props, this.props.onExit))
    }

    _getContent = color => {
        return this.props.babGame ? (
            <View>
                <Text style={ styles.title }>{i18n.t('joinTheGame')}</Text>
                <Text style={ [styles.team, { color }] }>Team { this.props.team }</Text>
                <TouchableOpacity onPress={ this._handlePlay }>
                    <View style={ styles.banner }>
                        <Icon.Ionicons
                            name={ icon('play-circle') }
                            style={ { color } }
                            color={ color }
                            size={ 180 }
                        />
                    </View>
                </TouchableOpacity>
            </View>
        ) : (
            <View>
                <Text style={ styles.title }>{i18n.t('pressStart')}</Text>
            </View>
        )
    }

    render() {
        const color = this.props.team === 'red' ? '#b97e7e' : '#9b9fd2'
        return (
            <View style={ styles.container }>
                <TouchableOpacity onPress={ this.props.onExit } style={ styles.close }>
                    <View>
                        <Icon.Ionicons
                            name={ icon('close') }
                            style={ styles.play }
                            color={ '#d0b0b9' }
                            size={ 40 }
                        />
                    </View>
                </TouchableOpacity>
                { this._getContent(color) }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    close: {
        position: 'absolute',
        top: 30,
        right: 30,
    },
    banner: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        marginBottom: 30,
        textAlign: 'center',
        fontFamily: 'lobster',
        fontSize: 50,
    },
    team: {
        paddingBottom: 20,
        fontFamily: 'lobster',
        textAlign: 'center',
        fontSize: 30,
    },
})

PlayComponent.propTypes = {
    bab: PropTypes.string,
    babGame: PropTypes.object,
    team: PropTypes.string,
    onExit: PropTypes.func,
}

export default PlayComponent
