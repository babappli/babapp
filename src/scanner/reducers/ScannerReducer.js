import ScannerConstants from 'babapp/src/scanner/constants/ScannerConstants'
import TeamDto from 'babapp/src/scanner/dto/TeamDto'
import GameDto from 'babapp/src/scanner/dto/GameDto'

export const ScannerStore = {
    team: [],
    game: null,
    babGame: null,
}

export function ScannerReducer(state = ScannerStore, action) {
    switch (action.type) {
        case ScannerConstants.RECEIVE_TEAM:
            return Object.assign({}, state, {
                team: action.team.map(t => TeamDto(t))
            })
        case ScannerConstants.RECEIVE_GAME:
            return Object.assign({}, state, {
                game: GameDto(action.game)
            })
        case ScannerConstants.RECEIVE_BAB_GAME:
            return Object.assign({}, state, {
                babGame: GameDto(action.babGame)
            })
        default:
            return state
    }
}
