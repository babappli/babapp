class _TeamDto {
    constructor(obj) {
        this.id = obj.id
        this.color = obj.color
        this.game_id = obj.game_id
        this.users_id = obj.users_id
    }
}

export default function TeamDto(team) {
    return new _TeamDto(Object.assign({ team: {} }, team))
}
