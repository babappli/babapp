class _GameDto {
    constructor(obj) {
        this.id = obj.id
        this.scoreblue = obj.scoreblue
        this.scorered = obj.scorered
        this.bab_id = obj.bab_id
        this.startedat = obj.startedat
        this.stoppedat = obj.stoppedat
    }
}

export default function GameDto(game) {
    return new _GameDto(Object.assign({ game: {} }, game))
}
