import Store from 'babapp/src/store/Store'
import ApiRoutes from 'babapp/src/navigation/constants/ApiRoutes'
import ScannerConstants from 'babapp/src/scanner/constants/ScannerConstants'

const availableBab = (params, cb = () => {}) => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.game(`?bab_id=eq.${params.bab}&stoppedat=is.null`))
        if (!res.ok && (res.status < 200 || res.status >= 300))
            throw new Error(res.statusText)
        const babGame = await res.json()
        if (babGame && babGame.length)
            dispatch(receiveBabGame(babGame[0]))
    } catch (err) {
        dispatch(fetchYourGame(Store.getState().AccountReducer.user.id))
    }
    cb()
}

const receiveBabGame = babGame => ({ type: ScannerConstants.RECEIVE_BAB_GAME, babGame })

const joinGame = (params, cb = () => {}) => async dispatch => {
    try {
        const resGame = await fetch(ApiRoutes.game(`?bab_id=eq.${params.bab}&stoppedat=is.null`))
        if (!resGame.ok && (resGame.status < 200 || resGame.status >= 300))
            throw new Error(resGame.statusText)
        let game = await resGame.json()
        if (!game.length) {
            const resPostGame = await fetch(ApiRoutes.game(), {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    scoreblue: 0,
                    scorered: 0,
                    bab_id: params.bab,
                })
            })
            const resGetGame = await fetch(ApiRoutes.game(`?bab_id=eq.${params.bab}&stoppedat=is.null`))
            if (!resGetGame.ok && (resGetGame.status < 200 || resGetGame.status >= 300))
                throw new Error(resGetGame.statusText)
            game = await resGetGame.json()
        }
        const gameid = game[0].id
        const resTeam = await fetch(ApiRoutes.team(), {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                color: params.team,
                game_id: gameid,
                users_id: Store.getState().AccountReducer.user.id
            })
        })
        if (!resTeam.ok && (resTeam.status < 200 || resTeam.status >= 300))
            throw new Error('Cannot launch the game')

        dispatch(fetchYourGame(Store.getState().AccountReducer.user.id))
    } catch (err) {
        dispatch(fetchYourGame(Store.getState().AccountReducer.user.id))
    }
    cb()
}

const receiveTeam = team => ({ type: ScannerConstants.RECEIVE_TEAM, team })

const fetchTeam = idGame => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.team(`?game_id=eq.${idGame}`))
        if (!res.ok && (res.status < 200 || res.status >= 300))
            throw new Error(res.statusText)

        const team = await res.json()
        if (!team)
            throw new Error(res.statusText)
        dispatch(receiveTeam(team))
    } catch (err) {
    }
}

const receiveGame = game => ({ type: ScannerConstants.RECEIVE_GAME, game })

const fetchGame = idGame => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.game(`?game_id=eq.${idGame}`))
        if (!res.ok && (res.status < 200 || res.status >= 300))
            throw new Error(res.statusText)

        const team = await res.json()
        if (!team)
            throw new Error(res.statusText)
        dispatch(receiveGame(team))
    } catch (err) {
    }
}

const fetchYourGame = (idUser, cb = () => {}) => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.gameTeam(`?users_id=eq.${idUser}`))
        if (!res.ok && (res.status < 200 || res.status >= 300))
            throw new Error(res.statusText)

        const game = await res.json()
        if (!game)
            throw new Error(res.statusText)
        if (game.length && game[0].team) {
            dispatch(receiveTeam(game[0].team))
            dispatch(receiveGame(game[0]))
        } else {
            dispatch(receiveTeam([]))
            dispatch(receiveGame(null))
        }
        cb()
    } catch (err) {
        cb()
    }
}

export default { joinGame, fetchYourGame, availableBab }
