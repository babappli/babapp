import {
    RECEIVE_USER
} from 'babapp/src/account/constants/AccountConstants'
import ApiRoutes from 'babapp/src/navigation/constants/ApiRoutes'

const receiveUser = user => ({ type: RECEIVE_USER, user })

const fetchUser = email => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.users(`?or=(email.eq.${email},login.eq.${email})`))
        if (!res.ok && (res.status < 200 || res.status >= 300))
            throw new Error(res.statusText)

        const json = await res.json()
        if (!json || !json.length)
            throw new Error(res.statusText)
        const user = json[0]
        dispatch(receiveUser(user))
    } catch (err) {
        console.log(err)
    }
}

const fetchUserById = id => async dispatch => {
    try {
        const res = await fetch(ApiRoutes.users(`?id=eq.${id}`))
        if (!res.ok && (res.status < 200 || res.status >= 300))
            throw new Error(res.statusText)

        const json = await res.json()
        if (!json || !json.length)
            throw new Error(res.statusText)
        const user = json[0]
        dispatch(receiveUser(user))
    } catch (err) {
        console.log(err)
    }
}

const updateUser = (id, params) => async dispatch => {
    try {
        await fetch(ApiRoutes.users(`?id=eq.${id}`), {
        	method: 'PATCH',
        	headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(params)
        })
        dispatch(fetchUserById(id))
    } catch (err) {
        console.log(err)
    }
}

export default { fetchUser, updateUser }
