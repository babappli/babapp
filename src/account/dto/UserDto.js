class _UserDto {
    constructor(obj) {
        this.id = obj.id
        this.email = obj.email
        this.login = obj.login
        this.pass = obj.pass
    }
}

export default function UserDto(user) {
    return new _UserDto(Object.assign({}, user))
}
