import React from 'react'
import {
    ScrollView,
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableOpacity,
} from 'react-native'
import i18n from 'i18n-js'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { icon } from 'babapp/src/utils/IconUtils'
import AccountHeader from 'babapp/src/account/components/AccountHeader'
import ListIcon from 'babapp/src/components/icons/ListIcon'
import SharedStyle from 'babapp/src/constants/SharedStyle'
import AuthActions from 'babapp/src/auth/actions/AuthActions'
import Store from 'babapp/src/store/Store'
import { PASSWORD, EMAIL } from 'babapp/src/navigation/constants/Routes'

class AccountScreen extends React.Component {
    static navigationOptions = {
        header: <AccountHeader />,
    }

    _renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={ item.onPress || this._onPress(item.link) }>
                <View style={ styles.listItem }>
                    <ListIcon name={ item.icon } style={ styles.listIcon }/>
                    <Text style={ styles.listText }>{ item.key }</Text>
                </View>
            </TouchableOpacity>
        )
    }

    _onPress = link => () => {
        this.props.navigation.navigate(link)
    }

    render() {
        return (
            <View style={ styles.content }>
                <View>
                    <FlatList
                        data={ [{
                            key: this.props.user.login,
                            link: '',
                            icon: icon('man'),
                        }, {
                            key: this.props.user.email,
                            link: '',
                            icon: icon('mail'),
                        }, {
                            key: '***********',
                            link: PASSWORD,
                            icon: icon('key')
                        }] }
                        scrollEnabled={false}
                        renderItem={ this._renderItem }
                    />
                </View>
                <View style={styles.listBottom}>
                    <TouchableOpacity onPress={ () => Store.dispatch(AuthActions.signout()) }>
                        <View style={ styles.listItem }>
                            <ListIcon name={ icon('exit') } style={ styles.listIcon }/>
                            <Text style={ styles.listText }>{i18n.t('signout')}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'space-between'
    },
    listItem: {
        flexDirection: 'row',
        borderBottomWidth: SharedStyle.borderWidth,
        borderBottomColor: SharedStyle.borderColor,
        paddingTop: 20,
        paddingBottom: 20,
    },
    listBottom: {
        borderTopWidth: SharedStyle.borderWidth,
        borderTopColor: SharedStyle.borderColor,
    },
    listIcon: {
        flex: 3,
        textAlign: 'center',
    },
    listText: {
        flex: 9,
        color: 'black',
    },
})

AccountScreen.propTypes = {
    user: PropTypes.object
}

const mapStateToProps = store => ({
    user: store.AccountReducer.user,
})

export default connect(mapStateToProps)(AccountScreen)
