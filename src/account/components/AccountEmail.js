import React from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
} from 'react-native'
import {
    SecureStore,
    Icon
} from 'expo'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import i18n from 'i18n-js'

import { icon } from 'babapp/src/utils/IconUtils'
import Button from 'babapp/src/components/buttons/Button'
import { EMAIL } from 'babapp/src/constants/GlobalConstants'
import ConnectionInput from 'babapp/src/components/form/ConnectionInput'
import Store from 'babapp/src/store/Store'
import AccountActions from 'babapp/src/account/actions/AccountActions'

class AccountEmail extends React.Component {
    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)
        this.state = {
            newMail: '',
            confirmNewMail: '',
        }
    }

    _handleChange = (attr, value) => {
        const obj = {}
        obj[attr] = value
        this.setState(obj)
    }

    _handleGoBack = () => {
        this.props.navigation.goBack()
    }

    _send = async () => {
        if(this.state.newMail && this.state.newMail == this.state.confirmNewMail) {
            Store.dispatch(AccountActions.updateUser(this.props.user.id, {email: this.state.newMail}))
            .then(() => {
                SecureStore.setItemAsync(EMAIL, this.state.newMail)
                this._handleGoBack()
            })
        }
    }

    render() {
        return(
            <View style={styles.container}>
                <TouchableOpacity onPress={ this._handleGoBack } style={ styles.close }>
                    <View>
                        <Icon.Ionicons
                            name={ icon('arrow-back') }
                            style={ styles.play }
                            color={ '#fff' }
                            size={ 30 }
                        />
                    </View>
                </TouchableOpacity>
                <View style={ styles.titleContainer }>
                    <Text style={ styles.title }>{i18n.t('changeEmail')}</Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.inputContainer}>
                          <ConnectionInput
                              style={styles.input}
                              onChangeText={t => this._handleChange('newMail', t)}
                              value= {this.state.newPassword}
                              placeholder={i18n.t('newEmail')}
                          />
                          <ConnectionInput
                            style={styles.input}
                            onChangeText={t => this._handleChange('confirmNewMail', t)}
                            value= {this.state.confirmNewPassword}
                            placeholder={i18n.t('confirmNewMail')}
                           />
                    </View>
                    <View style={ styles.bottomContainer } >
                        <Button
                            style={ styles.button }
                            onPress={this._send}
                            title={i18n.t('send')}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    close: {
        position: 'absolute',
        top: 45,
        left: 30,
        zIndex: 3,
    },
    titleContainer: {
        paddingTop: 100,
        paddingBottom: 90,
        position: 'relative',
        zIndex: 1,
        backgroundColor: '#d0b0b9',
    },
    title: {
        color: '#f1e7ea',
        fontSize: 50,
        fontFamily: 'lobster',
        textAlign: 'center',
        textShadowColor: 'rgba(0,0,0,0.4)',
        textShadowOffset: { widht: 1, height: 1 },
        textShadowRadius: 10,
    },
    content: {
        flex: 1,
        position: 'relative',
        zIndex: 2,
        top: -30,
        marginRight: 20,
        marginLeft: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: { widht: 0, height: 3 },
        shadowRadius: 10,
        shadowOpacity: 0.2,
        elevation: 4,
        bottom: 0,
    },
    inputContainer: {
        flex: 1,
        marginTop: 20,
        marginRight:20,
        marginLeft: 20,
    },
    input: {
        borderColor: 'transparent',
        backgroundColor: 'rgba(208, 176, 185, 0.3)',
        color: '#d0b0b9',
        borderWidth: 1
    },
    bottomContainer: {
        paddingRight: 50,
        paddingLeft: 50,
    },
    button: {
        borderWidth: 1,
        borderColor: 'transparent',
        backgroundColor: 'rgba(208, 176, 185, 0.1)',
        color: '#d0b0b9',
    },
})

AccountEmail.propTypes = {
    user: PropTypes.object
}

const mapStateToProps = store => ({
    user: store.AccountReducer.user,
})

export default connect(mapStateToProps)(AccountEmail)
