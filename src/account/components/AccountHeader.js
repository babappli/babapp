import React from 'react'
import {
    StyleSheet,
    View,
    Text,
} from 'react-native'
import i18n from 'i18n-js'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import ProfileIcon from 'babapp/src/components/icons/ProfileIcon'
import Layout from 'babapp/src/constants/Layout'
import SharedStyle from 'babapp/src/constants/SharedStyle'
import Colors from 'babapp/src/constants/Colors'

class AccountHeader extends React.Component {
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>{this.props.user.login}</Text>
                </View>
            </View>
        )
        // return (
        //     <View style={ styles.profile }>
        //         <View style={ styles.icon }>
        //             <ProfileIcon size={ Layout.window.width/7 }/>
        //         </View>
        //         <Text style={ styles.profileText }>{ this.props.user.login }</Text>
        //     </View>
        // )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    close: {
        position: 'absolute',
        top: 45,
        left: 30,
        zIndex: 3,
    },
    titleContainer: {
        paddingTop: 100,
        paddingBottom: 90,
        position: 'relative',
        zIndex: 1,
        backgroundColor: '#d0b0b9',
    },
    title: {
        color: '#f1e7ea',
        fontSize: 50,
        fontFamily: 'lobster',
        textAlign: 'center',
        textShadowColor: 'rgba(0,0,0,0.4)',
        textShadowOffset: { widht: 1, height: 1 },
        textShadowRadius: 10,
    },
})

AccountHeader.propTypes = {
    user: PropTypes.object
}

const mapStateToProps = store => ({
    user: store.AccountReducer.user,
})

export default connect(mapStateToProps)(AccountHeader)
