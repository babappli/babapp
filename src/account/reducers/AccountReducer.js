import AccountConstants from 'babapp/src/account/constants/AccountConstants'
import UserDto from 'babapp/src/account/dto/UserDto'

export const AccountStore = {
    user: UserDto(),
}

export function AccountReducer(state = AccountStore, action) {
    switch (action.type) {
        case AccountConstants.RECEIVE_USER:
            return Object.assign({}, state, {
                user: UserDto(action.user)
            })
        default:
            return state
    }
}
