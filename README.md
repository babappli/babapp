
# Babapp Mobile App

**Play** with your friends, **Join** a game, be the **best** player of your school!

## Getting started

>This project require [NodeJS](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/lang/en/), you need them to be installed !

 1. **Clone repository**
`git clone git@gitlab.com:babappli/babapp.git`
`cd babapp`
 2. **Install the dependencies**
`yarn`

 3. **Launch the dev server**
`yarn start`

## How to collaborate

 1. **Create** a branch
Named it  `YOUR-GIT-LOGIN_WORKING-FEATURE` by example `@tony.stark_flightStabilisation`

 2. **Develop** your feature

 3. **Push** on your Branch

 4. **Ask** a **merge request** to the `develop` branch

 5. **Assign** the review to your corresponding lead
